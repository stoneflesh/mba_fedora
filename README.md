# Macbook Air 2012-2017 Fedora
Post installation and booting into the OS (not live boot) There won't be WiFi, facetime camera won't work.


## Connecting to the internet

Options:
- Hotspot through USB
    - Mobile phone can be used for this
- MiniDisplay to Ethernet adapter

## Updating the system

`sudo dnf update
`

This will take a while...  
After the update is done, I recommend rebooting

`reboot
`

## Installing git 

`sudo dnf install git
`

## Downloading the scripts for installing FacetimeHD camera and Broadcom WiFi drivers

In terminal:

`git clone https://gitlab.com/stoneflesh/mba_fedora.git
`


## Installing Broadcom wifi drivers

`sudo bash mba_fedora/broadcom_wifi.sh
`

## Installing FacetimeHD 

`sudo bash mba_fedora/facetime_hd_camera.sh
`

If the kernel will be updated, the facetimeHD driver will need to be recompiled. To do that, run the same facetime_hd_camera script.
