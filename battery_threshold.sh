#!/bin/sh
TAG="$(git ls-remote --tags https://github.com/c---/applesmc-next | grep -oP 'tags/\K[0-9.]+' | tail -n1)"
git clone --depth 1 --branch "$TAG" https://github.com/c---/applesmc-next "/usr/src/applesmc-next-$TAG"
dkms install "applesmc-next/$TAG"

echo "To set threshold as super user:"
echo "echo 85 > /sys/class/power_supply/BAT0/charge_control_end_threshold"
echo "echo 83 > /sys/class/power_supply/BAT0/charge_control_full_threshold"
