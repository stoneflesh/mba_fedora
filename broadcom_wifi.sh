#!/bin/bash

# Check if the command is run by root or with sudo
if [[ $EUID -ne 0 ]]; then
   	echo "Must be root, use sudo before command"
   	exit 1
else
  # Clear the terminal
  clear
  echo "Installing Mackbook Air Wifi..."

  echo "Downloading proprietary wifi drivers"
  su -c 'dnf install -y http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm'

  echo "Installing the akmods and kernel-devel packages"
  sudo dnf install -y akmods "kernel-devel-uname-r == $(uname -r)"

  echo "Installing broadcom-wl package from the rpmfusion repo, which will install kmod-wl, akmod-wl, and other dependencies."
  sudo dnf install -y broadcom-wl

  echo "akmods to rebuild the kernel extension in the broadcom-wl package"
  sudo akmods

fi