#!/bin/bash

# Kernel relase version saved in a variable for printing later
KERNELRELEASE=$(uname -r)

# Check if the command is run by root or with sudo
if [[ $EUID -ne 0 ]]; then
  echo "Must be run with sudo."
  exit 1
else
  # If facetimehd.conf file exists, update
  if [ -d "/etc/modules-load.d/facetimehd.conf" ]; then
    cd ~/Documents
    git clone https://github.com/patjak/bcwc_pcie.git
    cd ~/Documents/bcwc_pcie/firmware
    make clean
    make
    make install
    cd ..
    make
    make install
    depmod
    modprobe facetimehd
  else
    # If facetimehd.conf doesn't exist, start from scratch
    echo "Installing dependencies"
    dnf install -y make gitv curl xzcat cpio kernel-devel

    echo "Installing FacetimeHD camera for $KERNELRELEASE"
    cd ~/Documents
    git clone https://github.com/patjak/bcwc_pcie.git
    cd bcwc_pcie/firmware
    make
    make install
    cd ..
    make
    make install
    depmod
    modprobe facetimehd

    echo "Making sure that module-load directory exists."
    if [ ! -d "/etc/modules-load.d" ]; then
      echo "Creating modules-load.d directory..."
      mkdir -p "/etc/modules-load.d"
    fi
    
    echo "Making sure that FacetimeHD is autoloaded every boot."
    if [ ! -d "/etc/modules-load.d/facetimehd.conf" ]; then
      echo "Adding facetimeHD to autoload"
      echo > "/etc/modules-load.d/facetimehd.conf" facetimehd
    fi
    echo "FacetimeHD installation finished."
  fi
fi
